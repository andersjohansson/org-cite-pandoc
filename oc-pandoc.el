;;; oc-pandoc.el --- Export org to pandoc markdown with citations  -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Anders Johansson

;; Author: Anders Johansson <mejlaandersj@gmail.com>
;; Created: 2021-06-23
;; Modified: 2021-06-23
;; Keywords: org, wp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'oc)
(require 'ox)
(require 'org-element)

(defun org-cite-pandoc--get-key (ref)
  "Return @key from REF object."
  (concat "@" (org-element-property :key ref)))

(defun org-cite-pandoc--format-citation (citation noauthor info)
  "Format CITATION object according to pandoc format.
NOAUTHOR non-nil for noauthor style.
INFO is the export state, as a property list."
  (org-export-data
   (org-cite-concat "["
                    (when noauthor "-")
                    ;; no support for global prefix or suffix
                    ;; (org-element-property :prefix citation)
                    (org-cite-mapconcat
                     (lambda (ref)
                       (org-cite-concat
                        (org-element-property :prefix ref)
                        (org-cite-pandoc--get-key ref)
                        (when-let ((suf (org-element-property :suffix ref)))
                          (org-cite-concat "," suf))))
                     (org-cite-get-references citation)
                     ";")
                    ;; (org-element-property :suffix citation)
                    "]")
   info))

(defun org-cite-pandoc-export-citation (citation style _ info)
  "Export CITATION object.
STYLE is the expected citation style, as a pair of strings or nil.  INFO is the
export communication channel, as a property list."
  (pcase style
    (`(,(or "text" "t") . ,_)
     ;; This would generate multiple in-text citations like:
     ;; AuthorA (2020), Author B (2021)
     (mapconcat #'org-cite-pandoc--get-key (org-cite-get-references citation) ", "))
    ;; "noauthor" style.
    (`(,(or "noauthor" "na") . ,_)
     (org-cite-pandoc--format-citation citation t info))
    ;; Default ("nil") style.
    (`(,_ . ,_)
     (org-cite-pandoc--format-citation citation nil info))
    ;; This should not happen.
    (_ (error "Invalid style: %S" style))))

(defun org-cite-pandoc-export-bibliography (_k _f _s _p backend _i)
  "Generate bibliography.
Just outputs a #refs section when BACKEND is markdown."
  (when (org-export-derived-backend-p backend 'md)
    "::: {#refs}\n:::"))


;;; Register processor
(org-cite-register-processor 'pandoc
  :export-citation #'org-cite-pandoc-export-citation
  :export-bibliography #'org-cite-pandoc-export-bibliography)

(provide 'org-cite-pandoc)
(provide 'oc-pandoc)
;;; oc-pandoc.el ends here

;; Local Variables:
;; nameless-current-name: "org-cite-pandoc"
;; End:
